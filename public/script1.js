const getItemsFromLocalStorage = () => {
    if (localStorage.getItem('name') !== null)
      $('#name').val(localStorage.getItem('name'));
    if (localStorage.getItem('email') !== null)
      $('#email').val(localStorage.getItem('email'));
    if (localStorage.getItem('message') !== null)
      $('#mesasge').val(localStorage.getItem('message'));
    if (localStorage.getItem('checkbox') !== null) {
      $('#check').prop('checked', localStorage.getItem('checkbox') === 'true');
      if ($('#checkbox').prop('checked')) $('#sendButton').removeAttr('disabled');
    }
  };
  
  const setItemsToLocalStorage = () => {
    localStorage.setItem('name', $('#name').val());
    localStorage.setItem('email', $('#email').val());
    localStorage.setItem('message', $('#message').val());
    localStorage.setItem('checkbox', $('#checkbox').prop('checked'));
  };
  
  const clearLocalStorage = () => {
    localStorage.clear();
    $('#name').val('');
    $('#email').val('');
    $('#message').val('');
    $('#checkbox').val(false);
  };
  $(document).ready(function () {
    getItemsFromLocalStorage();
    $('.openButton').click(() => {
      $('.hiddenDiv').css('display', 'flex');
      history.pushState(true, '', './form');
    });
    $('.closeButton').click(function () {
      $('.hiddenDiv').css('display', 'none');
      history.pushState(false, '', '.');
    });
    $('#form').submit(function (e) {
      e.preventDefault();
      $('.hiddenDiv').css('display', 'none');
      let data = $(this).serialize();
      let name;
      if ($('#name').val() !== '') name = $('#name').val();
      else name = 'user';
      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: 'https://formcarry.com/s/c4XS3E2Irt',
        data: data,
        success: function () {
          alert("Thanks for visit " + name);
          clearLocalStorage();
        },
      });
    });
    $('#checkbox').change(function () {
      if (this.checked) $('#sendButton').removeAttr('disabled');
      else $('#sendButton').attr('disabled', '');
    });
    $('#form').change(setItemsToLocalStorage);
  
    window.onpopstate = function (e) {
      if (e.state) $('.hiddenDiv').css('display', 'flex');
      else $('.hiddenDiv').css('display', 'none');
    };
  });
  